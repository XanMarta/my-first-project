﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    float playerSpeed = 50;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        playerMovement();
    }

    void playerMovement()
    {
        Vector2 velocity = new Vector2(0, 0);
        if (Input.GetKey(KeyCode.A))
        {
            velocity.x -= 1;
        }
        if (Input.GetKey(KeyCode.D))
        {
            velocity.x += 1;
        }
        if (Input.GetKey(KeyCode.W))
        {
            velocity.y += 1;
        }
        if (Input.GetKey(KeyCode.S))
        {
            velocity.y -= 1;
        }

        Vector3 position = new Vector3();
        position.x = transform.position.x + velocity.x * playerSpeed;
        position.y = transform.position.y + velocity.y * playerSpeed;
        transform.position.Set(position.x, position.y, position.z);

        Debug.Log("Velocity: " + velocity.ToString());
        Debug.Log("Position: " + position.ToString());
    }
}
